module.exports = {
  arrowParens: 'avoid',
  bracketSameLine: true,
  bracketSpacing: true,
  singleQuote: true,
  trailingComma: 'all',
  tabWidth: 2, // 缩进字节数
  useTabs: false, // 缩进不使用tab，使用空格
  semi: true, // 句尾添加分号
  disableLanguages: ['json', 'css', 'scss'],
  printWidth: 140,
  jsxSingleQuote: true,
  endOfLine: 'lf',
  'jsx-closing-bracket-location': 'line-aligned',
  cssUnitCase: 'perserve',
};
