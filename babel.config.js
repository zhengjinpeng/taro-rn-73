// babel-preset-taro 更多选项和默认值：
// https://github.com/NervJS/taro/blob/next/packages/babel-preset-taro/README.md
module.exports = {
  presets: [
    [
      'taro',
      {
        framework: 'react',
        ts: true,
        compiler: 'webpack5',
      },
    ],
  ],
  plugins: [
    [
      'transform-imports',
      {
        // "@/components": {
        //   "transform": "@/components/${member}",
        //   "preventFullImport": true
        // },
        'pd-taro-ui': {
          transform: 'pd-taro-ui/${member}',
          preventFullImport: true,
        },
        'pd-taro-form': {
          transform: 'pd-taro-form/${member}',
          preventFullImport: true,
        },
        lodash: {
          transform: 'lodash/${member}',
          preventFullImport: true,
        },
      },
    ],
  ],
};
