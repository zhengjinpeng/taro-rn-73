import type { UserConfigExport } from '@tarojs/cli';

export default {
  logger: {
    quiet: false,
    stats: true,
  },
  mini: {
    // webpackChain (chain, webpack) {
    //   chain.plugin('analyzer')
    //     .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])
    // }
  },
  h5: {
    webpackChain(chain, webpack) {
      // chain.merge({
      //   optimization: {
      //     // 开发模式下开启 Tree Shaking
      //     sideEffects: true,
      //     minimize: true,
      //   },
      // });
      // chain.plugin('analyzer').use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, []);
    },
  },
} satisfies UserConfigExport<'webpack5'>;
