import { defineConfig, type UserConfigExport } from '@tarojs/cli';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import devConfig from './dev';
import prodConfig from './prod';
import path from 'path';

// https://taro-docs.jd.com/docs/next/config#defineconfig-辅助函数
export default defineConfig<'webpack5'>(async (merge, { command, mode }) => {
  const baseConfig: UserConfigExport<'webpack5'> = {
    projectName: 'taro73',
    date: '2024-1-16',
    designWidth: 750,
    deviceRatio: {
      640: 2.34 / 2,
      750: 2.34 / 2,
      375: 2.34 / 2,
      828: 1.81 / 2,
    },
    sourceRoot: 'src',
    outputRoot: `dist/${process.env.TARO_ENV}`,
    plugins: [
      [
        'pd-taro-router',
        {
          test: 1,
        },
      ],
    ],
    defineConstants: {},
    copy: {
      patterns: [],
      options: {},
    },
    framework: 'react',
    compiler: {
      type: 'webpack5',
      // 仅 webpack5 支持依赖预编译配置
      prebundle: {
        enable: true,
        include: ['formik', 'big.js'],
        exclude: ['echarts', 'taro-charts'],
      },
    },
    cache: {
      enable: process.env.TARO_ENV !== 'rn', // Webpack 持久化缓存配置，建议开启。默认配置请参考：https://docs.taro.zone/docs/config-detail#cache
      buildDependencies: {
        config: [path.resolve(__dirname, '..', 'src/theme/theme.scss')],
      },
    },
    sass: {
      resource: [path.resolve(__dirname, '..', 'src/theme/theme.scss')],
    },
    alias: {
      '@/components': path.resolve(__dirname, '..', 'src/components'),
      '@/utils': path.resolve(__dirname, '..', 'src/utils'),
      '@/theme': path.resolve(__dirname, '..', 'src/theme'),
      '@/assets': path.resolve(__dirname, '..', 'src/assets'),
      '@/store': path.resolve(__dirname, '..', 'src/store'),
      '@/example': path.resolve(__dirname, '..', 'src/example'),
      '@/service': path.resolve(__dirname, '..', 'src/service'),
      'pd-taro-ui': path.resolve(__dirname, '..', 'src/pd-taro-ui'),
      'pd-taro-form': path.resolve(__dirname, '..', 'src/pd-taro-form'),
    },
    mini: {
      postcss: {
        pxtransform: {
          enable: true,
          config: {
            // selectorBlackList: ['.pt-'],
          },
        },
        url: {
          enable: true,
          config: {
            limit: 1024, // 设定转换尺寸上限
          },
        },
        cssModules: {
          enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
          config: {
            namingPattern: 'module', // 转换模式，取值为 global/module
            generateScopedName: '[name]__[local]___[hash:base64:5]',
          },
        },
      },
      webpackChain(chain) {
        chain.resolve.plugin('tsconfig-paths').use(TsconfigPathsPlugin);
      },
    },
    h5: {
      publicPath: '/',
      staticDirectory: 'static',
      output: {
        filename: 'js/[name].[hash:8].js',
        chunkFilename: 'js/[name].[chunkhash:8].js',
      },
      miniCssExtractPluginOption: {
        ignoreOrder: true,
        filename: 'css/[name].[hash].css',
        chunkFilename: 'css/[name].[chunkhash].css',
      },
      postcss: {
        pxtransform: {
          enable: true,
          config: {
            // selectorBlackList: ['.pt-'],
          },
        },
        autoprefixer: {
          enable: true,
          config: {},
        },
        cssModules: {
          enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
          config: {
            namingPattern: 'module', // 转换模式，取值为 global/module
            generateScopedName: '[name]__[local]___[hash:base64:5]',
          },
        },
      },
      webpackChain(chain) {
        chain.resolve.plugin('tsconfig-paths').use(TsconfigPathsPlugin);
      },
    },
    rn: {
      appName: 'taro73',
      output: {
        ios: './ios/main.jsbundle',
        iosAssetsDest: './ios',
        android: './android/app/src/main/assets/index.android.bundle',
        androidAssetsDest: './android/app/src/main/res',
        // iosSourceMapUrl: '',
        iosSourcemapOutput: './ios/main.map',
        // iosSourcemapSourcesRoot: '',
        // androidSourceMapUrl: '',
        androidSourcemapOutput: './android/app/src/main/assets/index.android.map',
        // androidSourcemapSourcesRoot: '',
      },
      postcss: {
        pxtransform: {
          enable: true,
          config: {
            // selectorBlackList: ['.pt-'],
          },
        },
        cssModules: {
          enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        },
      },
      resolve: {
        include: ['taro-design'],
      },
    },
  };
  if (process.env.NODE_ENV === 'development') {
    // 本地开发构建配置（不混淆压缩）
    return merge({}, baseConfig, devConfig);
  }
  // 生产构建配置（默认开启压缩混淆等）
  return merge({}, baseConfig, prodConfig);
});
