// themeToScss.js
const fs = require('fs');
const vm = require('vm');
const themeToScss = require("../src/theme/themeToScss.js")




// 指定要处理的文件路径
const filePath = 'src/theme/index.js';

// 读取原始文件内容
fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) throw err;

    // 删除export default theme字符串
    let modifiedCode = data.replace(/export default theme/g, '');
    vm.runInThisContext(modifiedCode);

    // 将 theme 数据转换为 SCSS 变量格式
    const scssVariables = themeToScss(theme)

    fs.writeFile('./src/theme/theme.scss', `${scssVariables}\n`, (err) => {
        if (err) throw err;
        console.log('Theme data has been successfully written to /src/theme/theme.scss');
    });
});

