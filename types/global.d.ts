/// <reference types="@tarojs/taro" />

declare module '*.png';
declare module '*.gif';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.styl';

declare namespace NodeJS {
  interface ProcessEnv {
    /** NODE 内置环境变量, 会影响到最终构建生成产物 */
    NODE_ENV: 'development' | 'production';
    /** 当前构建的平台 */
    TARO_ENV: 'weapp' | 'swan' | 'alipay' | 'h5' | 'rn' | 'tt' | 'quickapp' | 'qq' | 'jd';
    /**
     * 当前构建的小程序 appid
     * @description 若不同环境有不同的小程序，可通过在 env 文件中配置环境变量`TARO_APP_ID`来方便快速切换 appid， 而不必手动去修改 dist/project.config.json 文件
     * @see https://taro-docs.jd.com/docs/next/env-mode-config#特殊环境变量-taro_app_id
     */
    TARO_APP_ID: string;

    /** 当前运行打包环境 dev | prod ， 这个是自定义的，打包时请修改这个变量 */
    TARO_APP_MODE: string;
    /** 版本号 */
    TARO_APP_VERSION: string;
    /** 是否测试环境，为1时，就显示调试按钮，如自动填充假数据表单。 打包生产环境+build时，清空 */
    TARO_APP_TEST: string;

    /** 各环境的接口请求地址 */
    TARO_APP_DEV_FETCH_URL: string;
    /** 各环境的接口请求地址 */
    TARO_APP_PROD_FETCH_URL: string;
  }
}
