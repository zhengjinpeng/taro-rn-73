import { userConfig } from '@/theme/userConfig';

export const mapConfig = {
  appKeyAndroid: '',
  appKeyIos: '',
  h5Key: '',
  apiKey: '',
  ...userConfig.option.amap,
};
