export const initAppLog = async () => {};

export const log = (data: any) => {};

export const getLogText = async (): Promise<string> => '';

export const getLogFilePath = (): string => '';

export const copyLogFileToDownload = async (): Promise<string> => '';
