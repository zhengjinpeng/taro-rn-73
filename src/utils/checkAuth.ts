import useAuthStore from '@/store/useAuthStore';

/**
 * 校验权限
 * @param mode 默认模式：one 有其中一个符号就通过， all假如传数组，必须全部权限都有
 */
export default function checkAuth(auth: string | string[], mode: string = 'one', userAuthor: string[] = []) {
  if (!userAuthor?.length) {
    userAuthor = useAuthStore.getState().auths;
  }
  let bool = false;
  let auths: string[] = [];
  if (typeof auth === 'string') {
    auths.push(auth);
  } else if (Array.isArray(auth)) {
    auths = auth;
  }
  if (!auths.length) {
    bool = true;
    return bool;
  }
  if (mode === 'one') {
    userAuthor.forEach(v => {
      auths.forEach(v1 => {
        if (v.includes(v1)) {
          bool = true;
        }
      });
    });
  } else {
    let boolLength = 0;
    userAuthor.forEach(v => {
      auths.forEach(v1 => {
        if (v.includes(v1)) {
          boolLength++;
        }
      });
    });
    if (boolLength >= auths.length) {
      bool = true;
    }
  }
  // console.log('checkAuth', auth, bool);
  return bool;
}
