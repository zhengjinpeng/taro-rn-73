export interface RequestOption {
  url: string;
  data: Taro.request.Option['data'];
  header?: object;
  method?: Taro.request.Option['method'];
}
export interface RequestResponse<T> {
  success: boolean;
  msg: string;
  /** 接口返回里面的data */
  data: T;
  /** 接口返回的原始数据 */
  response: OriginResponse<T> | any;
}

export interface OriginResponse<T> {
  code: any;
  data: T;
}

export type RequestData = Pick<Taro.request.Option, 'url' | 'data' | 'header' | 'mode' | 'method'>;

export type UploadFileOption = Pick<Taro.uploadFile.Option, 'url' | 'name' | 'filePath' | 'formData'>;
