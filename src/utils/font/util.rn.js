import { loadAsync } from 'expo-font';
import 'react-native-url-polyfill/auto';

export const loadFont = (name, url) => {
  return loadAsync({
    [name]: url,
  });
};
