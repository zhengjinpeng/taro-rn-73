const userConfig = {
  option: {
    app: {
      permissions: {
        location: {
          title: '需要访问您的位置信息',
          content: 'APP将访问您的位置信息，以获取完整服务',
        },
      },
    },
    font: {
      local: {
        // Icon:true
      },
    },
    amap: {
      // 高德地图
      appKeyAndroid: '',
      appKeyIos: '',
      h5Key: '',
      apiKey: '',
    },
  },
};

export { userConfig };
