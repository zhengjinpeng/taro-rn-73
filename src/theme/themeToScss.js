const capitalizeFirstLetter = str => {
  if (typeof str !== 'string') {
    return '';
  }
  return str.charAt(0).toUpperCase() + str.slice(1);
};

const scssData = {
  '': [
    `// ---- 基础模块主题配置 ----

// 基础样式
$appPrimaryColor1: #1677ff;
$appSecondaryColor: #5bc0de;
$appSuccessColor: #34a853;
$appDangerColor: #ea4335;
$appWarningColor: #fbbc05;
$appPageColor: #fafbf8;
$appCustomColor1: #337ab7;
$appCustomColor2: #337ab7;
$appCustomColor3: #337ab7;

$appTextColor1: #373D52;
$appTextColor2: #73778E;
$appTextColor3: #A1A6B6;
$appTextColor4: #FFF;

$appTextSize1: 24px;
$appTextSize2: 26px;
$appTextSize3: 28px;
$appTextSize4: 30px;
$appTextSize5: 32px;
$appTextSize6: 34px;
$appTextSize7: 36px;
`,
  ],
  common: [
    `
// 公共属性
$appCommonRadius: 16px;`,
  ],
  header: [
    `
// Header组件
$appHeaderColor: #fff;
$appHeaderTextColor: #000;`,
  ],
  button: [
    `
// Button组件
$appButtonColor: #000;
$appButtonTextColor: #fff;
$appButtonSFontSize: 24px;
$appButtonSPadding: 20px;
$appButtonSHeight: 50px;
$appButtonMFontSize: 26px;
$appButtonMPadding: 25px;
$appButtonMHeight: 56px;
$appButtonLFontSize: 28px;
$appButtonLPadding: 30px;
$appButtonLHeight: 70px;`,
  ],
  tabBar: [
    `
// TabBar组件
$appTabBarNameColor: #666;
$appTabBarNameHoverColor: #000;`,
  ],
  tabs: [
    `
// Tab组件
$appTabLineWidth: 40px;
$appTabLineHeight: 8px;
$appTabLineRadius: 4px;`,
  ],
  avatar: [
    `
// Avatar组件
$appAvatarColor: #{$appPrimaryColor};
$appAvatarBgColor: #eee;
$appAvatarIconSize: 56px;
$appAvatarSSize: 64px;
$appAvatarMSize: 84px;
$appAvatarLSize: 120px;`,
  ],
  card: [
    `
// Card组件
$appCardRadius: #{$appCommonRadius};
$appCardMargin: 24px;`,
  ],
};

module.exports = theme => {
  Object.keys(theme).forEach(key => {
    const value = theme[key];
    if (typeof value !== 'object') {
      scssData[''].push(`$app${capitalizeFirstLetter(key)}: ${value};`);
    } else {
      switch (key) {
        case 'common': {
          if (value.redius) {
            scssData.common.push(`$appCommonRedius: ${value}px;`);
          }
          break;
        }
        case 'header': {
          if (value.color) {
            scssData.header.push(`$appHeaderColor: ${value.color};`);
          }
          if (value.textColor) {
            scssData.header.push(`$appHeaderTextColor: ${value.textColor};`);
          }
          break;
        }
        case 'button': {
          if (value.color) {
            scssData.button.push(`$appButtonColor: ${value.color};`);
          }
          if (value.textColor) {
            scssData.button.push(`$appButtonTextColor: ${value.textColor};`);
          }
          if (value.sizes) {
            Object.keys(value.sizes).forEach(size => {
              const { h, p, fs } = value.sizes[size];
              scssData.button.push(`$appButton${size.toUpperCase()}FontSize: ${fs}px;`);
              scssData.button.push(`$appButton${size.toUpperCase()}Padding: ${p}px;`);
              scssData.button.push(`$appButton${size.toUpperCase()}Height: ${h}px;`);
            });
          }
          break;
        }
        case 'tabBar': {
          if (value.nameColor) {
            scssData.tabBar.push(`$appTabBarNameColor: ${value.nameColor};`);
          }
          if (value.nameHoverColor) {
            scssData.tabBar.push(`$appTabBarNameHoverColor: ${value.nameHoverColor};`);
          }
          break;
        }
        case 'tab': {
          if (value.lineWidth) {
            scssData.tabs.push(`$appTabLineWidth: ${value.lineWidth}px;`);
          }
          if (value.lineHeight) {
            scssData.tabs.push(`$appTabLineHeight: ${value.lineHeight}px;`);
          }
          if (value.lineRadius) {
            scssData.tabs.push(`$appTabLineRadius: ${value.lineRadius}px;`);
          }
          break;
        }
        case 'avatar': {
          if (value.color) {
            scssData.avatar.push(`$appAvatarColor: ${value.color};`);
          }
          if (value.bgColor) {
            scssData.avatar.push(`$appAvatarBgColor: ${value.bgColor};`);
          }
          if (value.iconSize) {
            scssData.avatar.push(`$appAvatarIconSize: ${value.iconSize}px;`);
          }
          if (value.sizes?.s) {
            scssData.avatar.push(`$appAvatarSSize: ${value.sizes.s};`);
          }
          if (value.sizes?.m) {
            scssData.avatar.push(`$appAvatarMSize: ${value.sizes.m};`);
          }
          if (value.sizes?.l) {
            scssData.avatar.push(`$appAvatarLSize: ${value.sizes.l};`);
          }
          break;
        }
        case 'card': {
          if (value.radius !== undefined) {
            scssData.card.push(`$appCardRadius: ${value.radius}px;`);
          }
          break;
        }
      }
    }
  });
  return Object.keys(scssData)
    .map(key => {
      scssData[key].splice(1, 0, '\n// 用户样式');
      return scssData[key].join('\n');
    })
    .join('\n');
};
