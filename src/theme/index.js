/**
 * 默认主题配置
 */

const getConfig = {
  radiusValue: 16,
  radiusPxValue: null,
};

const defaultTheme = {
  // General
  primaryColor: '#1677ff', // 主色
  secondaryColor: '#5bc0de', // 辅色
  successColor: '#34a853',
  dangerColor: '#ea4335',
  warningColor: '#fbbc05',
  pageColor: '#fafbf8',

  //用户自定义颜色1
  customColor1: '#337ab7',
  customColor2: '#337ab7',
  customColor3: '#337ab7',

  // 文本颜色 从暗色到亮色
  textColor1: '#373D52',
  textColor2: '#73778E',
  textColor3: '#A1A6B6',
  textColor4: '#FFF',

  // 文本尺寸 从小到大
  textSize1: '24px',
  textSize2: '26px',
  textSize3: '28px',
  textSize4: '30px',
  textSize5: '32px',
  textSize6: '34px',
  textSize7: '36px',
  // 公共配置
  common: {
    get radius() {
      if (!getConfig.radiusPxValue) {
        getConfig.radiusPxValue = Taro.pxTransform(getConfig.radiusValue);
      }
      return getConfig.radiusPxValue;
    },
    set radius(value) {
      getConfig.radiusValue = value;
    },
    get radiusValue() {
      return getConfig.radiusValue;
    },
  },
};

const theme = {
  ...defaultTheme,

  header: {
    color: '#fff', // 仅支持rgb hex值，请勿使用纯单词 设置为数组将显示一个渐变按钮
    textColor: '#000', // 文本颜色
    showWechat: false, // 微信公众号是否显示header
    showWap: true, // h5是否显示header
  },

  button: {
    // color: '#000',
    radiusType: 'round-min', // 按钮圆角类型 square直角 round圆角 round-min较小的圆角
    size: 'm', // 按按钮尺寸 s m l
    plain: false, // 是否镂空
    sizes: {
      s: { fs: 24, p: 20, h: 50 },
      m: { fs: 28, p: 30, h: 70 },
      l: { fs: 32, p: 40, h: 90 },
    },
  },

  tabBar: {
    nameColor: '#666',
    nameHoverColor: '#000',
  },

  tab: {
    lineWidth: 30,
    lineHeight: 8,
    lineRadius: 4,
  },

  tag: {
    radiusType: 'round-min', // 按钮圆角类型 square直角 round圆角 round-min较小的圆角
  },

  avatar: {
    size: 'm',
    color: defaultTheme.primaryColor,
    bgColor: '#eee',
    radiusType: 'round', // 按钮圆角类型 square直角 round圆角 round-min较小的圆角
    iconSize: 64,
    sizes: {
      s: '64px',
      m: '84px',
      l: '120px',
    },
  },

  card: {
    shadow: true,
    radius: defaultTheme.common.radiusValue,
    margin: 24,
  },

  image: {
    radiusType: 'square', // 按钮圆角类型 square直角 round-min较小的圆角
  },

  cardSelect: {
    color: defaultTheme.primaryColor,
    radiusType: 'round-min', // 按钮圆角类型 square直角 round圆角 round-min较小的圆角
  },
};

export default theme;
