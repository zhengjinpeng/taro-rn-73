import { Header, ScrollView, TopView, GroupList, Text, LongPress, Column } from 'pd-taro-ui';
import { toast } from '@/utils/util';

export default function LongPressExample() {
  return (
    <TopView>
      <Header title='LongPress' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='长按事件'>
            <LongPress onLongPress={() => toast('长按事件')}>
              <Column style={{ backgroundColor: '#fff', padding: 12 }}>
                <Text>长按此区域</Text>
              </Column>
            </LongPress>
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
}
