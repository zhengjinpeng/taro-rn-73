export type PageParams = {
  testId: string;
};

export type PageData = {
  testData: object;
};

export type PageBackData = {};

export const middlewareConfig = {};
