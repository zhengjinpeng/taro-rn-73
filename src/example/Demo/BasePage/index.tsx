import Taro, { useDidHide, useDidShow, useLoad, useRouter, useUnload } from '@tarojs/taro';
import { Button, Text, View } from '@tarojs/components';
import { Cell, GroupList, Header, ScrollView, TopView } from 'pd-taro-ui';
import { PageParams, PageData, PageBackData } from './route.config';
import { navigateTo, redirectTo, reLaunch, useRouterData, navigateBack } from '@/utils/router';
import './index.scss';

const prefix = 'TODOBasePage__';

const BasePage = () => {
  const router = useRouterData();
  const pageData = router.data as PageData;
  const pageParams = router.params as PageParams;
  const pageId = router.pageId;
  useLoad(() => {
    console.log('BasePage onLoad');
  });

  useUnload(() => {
    console.log('BasePage onUnload');
  });

  useDidShow(() => {
    console.log('BasePage componentDidShow');
  });

  useDidHide(() => {
    console.log('BasePage componentDidHide');
  });

  return (
    <TopView>
      <Header title='基础页面+生命周期' titleCenter />
      <ScrollView>
        <View style={{ padding: 12 }}>
          <Text>路由参数params：{JSON.stringify(pageParams)}</Text>
        </View>
        <View style={{ padding: 12 }}>
          <Text>路由参数data(一般不需要用到)：{JSON.stringify(pageData)}</Text>
        </View>
        <View style={{ padding: 12 }}>
          <Text>router id：{JSON.stringify(pageId)}</Text>
        </View>
        <GroupList>
          <GroupList.Item title='函数'>
            <Cell.Group>
              <Cell
                title='页面生命周期+navigateTo， param传参'
                onClick={() =>
                  navigateTo({
                    url: '/example/Demo/BasePage/index',
                    params: { testId: Math.random().toString() },
                  })
                }
                isLink
              />
              <Cell
                title='navigateTo， data传参'
                onClick={() =>
                  navigateTo({
                    url: '/example/Demo/BasePage/index',
                    params: { testId: Math.random().toString() },
                    data: { testData: { a: '我是data' } },
                  })
                }
                isLink
              />
              <Cell
                title='navigateTo并获取回调参数'
                onClick={() =>
                  navigateTo({
                    url: '/example/Demo/BackPage/index',
                    onBack: res => {
                      console.log('获取回调参数', res);
                    },
                  })
                }
                isLink
              />
              <Cell
                title='redirectTo'
                onClick={() =>
                  redirectTo({
                    url: '/example/Demo/BasePage/index',
                    params: { testId: 'redirectTo' },
                  })
                }
                isLink
              />
              <Cell
                title='reLaunch首页'
                onClick={() =>
                  reLaunch({
                    url: '/pages/index/index',
                  })
                }
                isLink
              />
              <Cell title='navigateBack返回上一页' onClick={() => navigateBack()} isLink />
              <Cell title='navigateBack返回数据' onClick={() => navigateBack()} isLink />
            </Cell.Group>
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
};
export default BasePage;
