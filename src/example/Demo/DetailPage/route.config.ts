export type PageParams = {
  pageType: 'add' | 'edit' | 'look';
  id?: string;
};

export const pageTypeTexts = {
  add: '新增',
  edit: '编辑',
  look: '查看',
};

export type PageData = {};

export type PageBackData = {};

export const middlewareConfig = {};
