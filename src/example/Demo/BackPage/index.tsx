import Taro, { useDidHide, useDidShow, useLoad, useRouter, useUnload } from '@tarojs/taro';
import { Input, Text, View } from '@tarojs/components';
import { Button, Header, ScrollView, TopView } from 'pd-taro-ui';
import { PageParams, PageData, PageBackData } from './route.config';
import { navigateTo, redirectTo, reLaunch, useRouterData, navigateBack, setPageBackData } from '@/utils/router';
import { useState } from 'react';

const BasePage = () => {
  const router = useRouterData();
  const pageData = router.data as PageData;
  const pageParams = router.params as PageParams;
  const [value, setValue] = useState('这是回调值，可修改');

  return (
    <TopView>
      <Header title='回调页面' titleCenter />
      <ScrollView>
        <Input value={value} onInput={e => setValue(e.detail.value)} />
        <Button
          size='l'
          onClick={() => {
            const backData: PageBackData = {
              backStr: value,
            };
            setPageBackData(backData);
            navigateBack();
          }}>
          提交并返回
        </Button>
      </ScrollView>
    </TopView>
  );
};
export default BasePage;
