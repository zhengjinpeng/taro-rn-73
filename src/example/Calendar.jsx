import { Header, ScrollView, TopView, GroupList, Calendar } from 'pd-taro-ui';
import dayjs from 'dayjs';
import { useState } from 'react';
import YearView from 'pd-taro-ui/Calendar/YearView';
import MonthView from 'pd-taro-ui/Calendar/MonthView';

export default function ButtonExample() {
  const [dayStr, setDayStr] = useState('');
  const [dayRange, setDayRange] = useState(['2024-01-30', '2024-01-31']);
  const [yearStr, setYearStr] = useState('2024');
  const [yearRange, setYearRange] = useState(['2024', '2025']);
  const [monthStr, setMonthStr] = useState('');
  const [monthRange, setMonthRange] = useState(['2024-01', '2024-02']);

  return (
    <TopView>
      <Header title='Calendar' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='天选择'>
            <Calendar
              mode='day'
              min={dayjs().subtract(3, 'days').format('YYYY-MM-DD')}
              value={dayStr}
              onChange={v => {
                setDayStr(v);
                console.log(v);
              }}
            />
          </GroupList.Item>
          <GroupList.Item title='范围选择'>
            <Calendar
              mode='range'
              value={dayRange}
              onChange={v => {
                setDayRange(v);
                console.log(v);
              }}
            />
          </GroupList.Item>
          <GroupList.Item title='年份选择'>
            <YearView
              mode='day'
              value={yearStr}
              onChange={v => {
                setYearStr(v);
                console.log(v);
              }}
            />
          </GroupList.Item>
          <GroupList.Item title='年份范围选择'>
            <YearView
              mode='range'
              value={yearRange}
              onChange={v => {
                setYearRange(v);
                console.log(v);
              }}
            />
          </GroupList.Item>
          <GroupList.Item title='月份选择'>
            <MonthView
              mode='day'
              value={monthStr}
              onChange={v => {
                setMonthStr(v);
                console.log(v);
              }}
            />
          </GroupList.Item>
          <GroupList.Item title='月份范围选择'>
            <MonthView
              mode='range'
              value={monthRange}
              onChange={v => {
                setMonthRange(v);
                console.log(v);
              }}
            />
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
}
