import { Header, ScrollView, TopView, GroupList, Menu, Text, Button, Column } from 'pd-taro-ui';
import { useRef } from 'react';
import { toast } from '@/utils/util';

export default function MenuExample() {
  const itemRef = useRef();

  return (
    <TopView>
      <Header title='Menu' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='基础用法'>
            <Menu round>
              <Menu.Item
                title='显示1'
                options={[
                  { name: '标题1', value: 1 },
                  { name: '标题2', value: 2 },
                ]}
              />
              <Menu.Item
                column={2}
                title='多列'
                options={[
                  { name: 'ABC1', value: 1 },
                  { name: 'ABC2', value: 2 },
                  { name: 'ABC3', value: 3 },
                ]}
              />
              <Menu.Item title='自定义内容' ref={itemRef}>
                <Text>自定义显示</Text>
                <Button onClick={() => itemRef.current.toggle()}>关闭</Button>
              </Menu.Item>
            </Menu>
          </GroupList.Item>

          <GroupList.Item title='直角'>
            <Menu>
              <Menu.Item
                title='允许取消'
                cancel
                options={[
                  { name: '标题1', value: 1 },
                  { name: '标题2', value: 2 },
                ]}
              />
              <Menu.Item
                column={2}
                align='center'
                title='居中对齐'
                options={[
                  { name: 'ABC1', value: 1 },
                  { name: 'ABC2', value: 2 },
                  { name: 'ABC3', value: 3 },
                ]}
              />
              <Menu.Item title='自定义内容' ref={itemRef}>
                <Text>自定义显示</Text>
                <Button onClick={() => itemRef.current.toggle()}>关闭</Button>
              </Menu.Item>
            </Menu>
          </GroupList.Item>
          <GroupList.Item title='点击事件'>
            <Menu>
              <Menu.Item
                title='菜单1'
                options={[
                  { name: '标题1', value: 1 },
                  { name: '标题2', value: 2 },
                ]}
              />
              <Menu.Item
                title='菜单2'
                options={[
                  { name: 'ABC1', value: 1 },
                  { name: 'ABC2', value: 2 },
                  { name: 'ABC3', value: 3 },
                ]}
              />
              <Menu.Item title='点击菜单' ref={itemRef} onClick={() => toast('点击了菜单')}></Menu.Item>
            </Menu>
          </GroupList.Item>
        </GroupList>
        <Column style={{ height: 800 }} />
      </ScrollView>
    </TopView>
  );
}
