import { Header, ScrollView, TopView, GroupList, Cell } from 'pd-taro-ui';
import { Switch } from 'pd-taro-form';
import { useState } from 'react';

export default function SwitchExample() {
  const [value, setValue] = useState(true);
  const [strValue, setStrValue] = useState('1');
  return (
    <TopView>
      <Header title='Switch' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='基础用法'>
            <Switch value={value} onChange={e => setValue(e)} />
          </GroupList.Item>
          <GroupList.Item title='静态+自定义文案'>
            <Switch value={value} onChange={e => setValue(e)} staticed />
            <Switch value={value} onChange={e => setValue(e)} staticed trueText='开' falseText='关' />
          </GroupList.Item>
          <GroupList.Item title='自定义value类型'>
            <Switch value={strValue} onChange={e => setStrValue(e)} trueValue='1' falseValue='0' />
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
}
