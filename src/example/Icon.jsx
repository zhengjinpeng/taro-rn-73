import { View, Text } from '@tarojs/components';

import { Icon, Space, Header, ScrollView, TopView, GroupList, Grid, Column } from 'pd-taro-ui';
import Taro from '@tarojs/taro';
import { iconNames } from 'pd-taro-ui/Icon';

export default function IconExample() {
  return (
    <TopView>
      <Header title='Icon' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='大小'>
            <Icon name='warning' size={72} />
          </GroupList.Item>
          <GroupList.Item title='颜色' desc=''>
            <Space row>
              <Icon name='close' color='red' />
              <Icon name='arrow-up-filling' color='blue' />
            </Space>
          </GroupList.Item>
          <GroupList.Item title='MyIcon'>
            <Grid gap={20} square>
              {iconNames.map(item => {
                return (
                  <Column
                    key={item}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                    onClick={() => {
                      Taro.setClipboardData({
                        data: `<Icon name="${item}" size={72} />`,
                      }).then(() => {
                        Taro.showToast({
                          title: '已复制到剪切板',
                          icon: 'none',
                        });
                      });
                    }}>
                    <Icon name={item} size={72} />
                    <Text style={{ fontSize: 14 }}>{item}</Text>
                  </Column>
                );
              })}
            </Grid>
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
}
