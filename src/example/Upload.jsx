import { Header, ScrollView, TopView, Card, Divider } from 'pd-taro-ui';
// import UploadFile from 'pd-taro-form/UploadFile';
import { View } from '@tarojs/components';

export default function UploadExample() {
  return (
    <TopView>
      <Header title='Upload' />
      <ScrollView>
        <Card margin verticalPadding={false}>
          <Divider.Group padding={0}>
            <View>单图</View>
            {/* <UploadImage /> */}
            <View>多图</View>
            {/* <UploadImages /> */}
          </Divider.Group>
        </Card>
      </ScrollView>
    </TopView>
  );
}
