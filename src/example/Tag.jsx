import { Cell, Tag, Header, ScrollView, TopView, GroupList } from 'pd-taro-ui';
import { useState } from 'react';

export default function TagExample() {
  const [list, setList] = useState(['a', 'b', 'c']);
  return (
    <TopView>
      <Header title='Tag' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='基础用法'>
            <Cell.Group line={false}>
              <Cell title='default' desc={<Tag type='default'>标签</Tag>} />
              <Cell title='primary' desc={<Tag type='primary'>标签</Tag>} />
              <Cell title='success' desc={<Tag type='success'>标签</Tag>} />
              <Cell title='danger' desc={<Tag type='danger'>标签</Tag>} />
              <Cell title='warning' desc={<Tag type='warning'>标签</Tag>} />
              <Cell
                title='default'
                desc={
                  <Tag type='default' plain>
                    标签
                  </Tag>
                }
              />
              <Cell
                title='primary'
                desc={
                  <Tag type='primary' plain>
                    标签
                  </Tag>
                }
              />
              <Cell
                title='success'
                desc={
                  <Tag type='success' plain>
                    标签
                  </Tag>
                }
              />
              <Cell
                title='danger'
                desc={
                  <Tag type='danger' plain>
                    标签
                  </Tag>
                }
              />
              <Cell
                title='warning'
                desc={
                  <Tag type='warning' plain>
                    标签
                  </Tag>
                }
              />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='圆角'>
            <Cell.Group line={false}>
              <Cell title='直角' desc={<Tag radiusType='square'>标签</Tag>} />
              <Cell title='小圆角' desc={<Tag radiusType='round-min'>标签</Tag>} />
              <Cell title='圆角' desc={<Tag radiusType='round'>标签</Tag>} />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='尺寸'>
            <Cell.Group line={false}>
              <Cell title='s' desc={<Tag size='s'>标签</Tag>} />
              <Cell title='m' desc={<Tag size='m'>标签</Tag>} />
              <Cell title='l' desc={<Tag size='l'>标签</Tag>} />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='可关闭'>
            <Cell.Group line={false}>
              {list.map((item, index) => {
                return (
                  <Cell
                    key={index}
                    desc={
                      <Tag
                        closable
                        onClose={() => {
                          setList(list.filter((_v, i) => i !== index));
                        }}>
                        {item}
                      </Tag>
                    }
                  />
                );
              })}
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='其他'>
            <Cell.Group line={false}>
              <Cell
                title='default'
                desc={
                  <Tag plain texColor='#666'>
                    标签
                  </Tag>
                }
              />
              <Cell title='primary' desc={<Tag plain>标签</Tag>} />
              <Cell title='success' desc={<Tag>标签</Tag>} />
            </Cell.Group>
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
}
