import { TopView, Header, ScrollView, Text } from 'pd-taro-ui';
import TabBar, { TabBarProps } from 'pd-taro-ui/TabBar';
import { useState } from 'react';

export default function TabBarExample() {
  const tabs: TabBarProps['tabs'] = [
    {
      key: '1',
      name: '首页',
      icon: 'home',
      onClick: () => {
        setTabKey('1');
      },
    },
    {
      key: '2',
      name: '页面2',
      icon: 'home',
      onClick: () => {
        setTabKey('2');
      },
    },
    {
      key: '3',
      name: '页面3',
      icon: 'home',
      onClick: () => {
        setTabKey('3');
      },
    },
  ];
  const [tabKey, setTabKey] = useState(tabs[0].key);
  return (
    <TopView isSafe>
      <Header title='TabBar' />
      <ScrollView>
        <TabBar tabs={tabs} focusKey={tabKey}></TabBar>
      </ScrollView>
    </TopView>
  );
}
