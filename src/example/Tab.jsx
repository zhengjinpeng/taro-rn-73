import { Tab, Header, ScrollView, TopView, GroupList } from 'pd-taro-ui';
import { View, Swiper, SwiperItem } from '@tarojs/components';
import { useState } from 'react';

export default function TabExample() {
  const [activeKey, setActiveKey] = useState(1);

  const [swipeActiveKey, setSwipeActiveKey] = useState(0);

  return (
    <TopView>
      <Header title='Tab' />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='基础用法'>
            <Tab value={activeKey} onChange={setActiveKey}>
              <Tab.Item title='标题1' paneKey={1}>
                <View>内容1</View>
              </Tab.Item>
              <Tab.Item title='标题2' paneKey={2}>
                <View>内容2</View>
              </Tab.Item>
              <Tab.Item title='标题3' paneKey={3}>
                <View>内容3</View>
              </Tab.Item>
            </Tab>
          </GroupList.Item>
          <GroupList.Item title='仅使用导航部分'>
            <Tab>
              <Tab.Item title='标题1' paneKey={1} />
              <Tab.Item title='标题2' paneKey={2} />
              <Tab.Item title='标题3' paneKey={3} />
            </Tab>
          </GroupList.Item>
          <GroupList.Item title='滚动'>
            <Tab scroll>
              <Tab.Item title='标题1' paneKey={1} />
              <Tab.Item title='标题2' paneKey={2} />
              <Tab.Item title='标题3' paneKey={3} />
              <Tab.Item title='标题4' paneKey={4} />
              <Tab.Item title='标题5' paneKey={5} />
              <Tab.Item title='标题6' paneKey={6} />
              <Tab.Item title='标题7' paneKey={7} />
            </Tab>
          </GroupList.Item>
          <GroupList.Item title='按钮样式1'>
            <Tab type='button'>
              <Tab.Item title='标题1' />
              <Tab.Item title='标题2' />
              <Tab.Item title='标题3' />
              <Tab.Item title='标题4' />
            </Tab>
          </GroupList.Item>
          <GroupList.Item title='按钮样式2'>
            <Tab type='button' buttonRound>
              <Tab.Item title='标题1' />
              <Tab.Item title='标题2' />
              <Tab.Item title='标题3' />
              <Tab.Item title='标题4' />
            </Tab>
          </GroupList.Item>
          <GroupList.Item title='展开更多+swiper'>
            <Tab scroll expand type='button' buttonRound value={swipeActiveKey} onChange={setSwipeActiveKey}>
              <Tab.Item title='标题1' />
              <Tab.Item title='标题2' />
              <Tab.Item title='标题3' />
              <Tab.Item title='标题4' />
              <Tab.Item title='标题5' />
              <Tab.Item title='标题6' />
              <Tab.Item title='标题7' />
            </Tab>
            <Swiper
              duration={300}
              current={Number(swipeActiveKey)}
              onChange={event => {
                setSwipeActiveKey(event.detail.current);
              }}>
              <SwiperItem>
                <View style={{ backgroundColor: 'grey', height: 400 }}>内容1</View>
              </SwiperItem>
              <SwiperItem>
                <View style={{ backgroundColor: 'blue', height: 400 }}>内容2</View>
              </SwiperItem>
              <SwiperItem>
                <View style={{ backgroundColor: 'red', height: 400 }}>内容3</View>
              </SwiperItem>
              <SwiperItem>
                <View style={{ backgroundColor: 'red', height: 400 }}>内容4</View>
              </SwiperItem>
              <SwiperItem>
                <View style={{ backgroundColor: 'red', height: 400 }}>内容5</View>
              </SwiperItem>
              <SwiperItem>
                <View style={{ backgroundColor: 'red', height: 400 }}>内容6</View>
              </SwiperItem>
              <SwiperItem>
                <View style={{ backgroundColor: 'red', height: 400 }}>内容7</View>
              </SwiperItem>
            </Swiper>
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
}
