import { Textarea as TaroTextarea, View, Text } from '@tarojs/components';
import theme from '@/theme';
import { ComponentProps, useState } from 'react';
import './index.scss';
import classNames from 'classnames';

interface TextareaProps {
  placeholder?: string;
  value?: string;
  onChange?: (value: string) => void;
  onBlur?: any;
  staticed?: boolean;
  disabled?: boolean;
  /** 是否显示计数器 */
  showCounter?: boolean;
  maxlength?: number;
  compProps?: ComponentProps<typeof TaroTextarea>;
}
const Textarea = ({
  onChange,
  value,
  disabled,
  staticed,
  onBlur,
  showCounter = false,
  placeholder,
  maxlength = -1,
  compProps,
}: TextareaProps) => {
  const [hover, setHover] = useState(false);
  if (staticed) {
    return <View className='pd-textarea--static'>{value}</View>;
  }
  return (
    <View className={classNames({ 'pd-textarea': true, 'pd-textarea--disabled': disabled, 'pd-textarea--hover': hover })}>
      <TaroTextarea
        className='pd-textarea__inner'
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        {...compProps}
        onInput={e => onChange?.(e.detail.value)}
        onFocus={e => {
          setHover(true);
          compProps?.onFocus?.(e);
        }}
        onBlur={e => {
          setHover(false);
          onBlur(e);
        }}
        maxlength={maxlength}
      />
      {showCounter && (
        <Text className='pd-textarea__counter'>
          {value?.length || 0}
          {maxlength !== 0 && maxlength !== -1 && `/${maxlength}`}
        </Text>
      )}
    </View>
  );
};
export default Textarea;
