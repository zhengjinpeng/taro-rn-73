import { Input as TaroInput, View } from '@tarojs/components';
import theme from '@/theme';
import { ComponentProps, useState } from 'react';
import './index.scss';
import classNames from 'classnames';
import { Icon } from 'pd-taro-ui';

interface InputProps {
  placeholder?: string;
  value?: string;
  onChange?: (value: string) => void;
  onBlur?: any;
  staticed?: boolean;
  disabled?: boolean;
  compProps?: ComponentProps<typeof TaroInput>;
  clearable?: boolean;
  className?: string;
  /** 前缀 */
  renderPrefix?: any;
  prefixClassName?: string;
  /** 后缀 */
  renderSuffix?: any;
  suffixClassName?: string;
}
const Input = ({
  onChange,
  value,
  disabled,
  staticed,
  onBlur,
  clearable = false,
  placeholder,
  className,
  compProps,
  renderPrefix,
  renderSuffix,
  prefixClassName,
  suffixClassName,
}: InputProps) => {
  const [hover, setHover] = useState(false);
  if (staticed) {
    return <View className='pd-input--static'>{value}</View>;
  }
  return (
    <View className={classNames({ 'pd-input': true, 'pd-input--disabled': disabled, 'pd-input--hover': hover }, className)}>
      {!!renderPrefix && <View className={classNames('pd-input__prefix', prefixClassName)}>{renderPrefix}</View>}
      <TaroInput
        className='pd-input__inner'
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        {...compProps}
        onInput={e => onChange?.(e.detail.value)}
        onFocus={e => {
          setHover(true);
          compProps?.onFocus?.(e);
        }}
        onBlur={e => {
          setHover(false);
          onBlur?.(e);
        }}
      />
      {!!renderSuffix && <View className={classNames('pd-input__suffix', suffixClassName)}>{renderSuffix}</View>}
      {clearable && !disabled && !!value && <Icon name='error' size={36} color='#bbb' onClick={() => onChange?.('')} />}
    </View>
  );
};
export default Input;
