import { View } from '@tarojs/components';
import theme from '@/theme';
import { ComponentProps } from 'react';
import SwitchComp from './SwitchComp';
import './index.scss';

interface SwitchProps {
  placeholder?: string;
  value?: number | string | boolean;
  onChange?: (v: number | string | boolean) => void;
  onBlur?: any;
  staticed?: boolean;
  disabled?: boolean;
  compProps?: ComponentProps<typeof TaroSwitch>;
  /** 表示真值，默认true */
  trueValue?: number | string | boolean;
  /** 表示假值，默认false */
  falseValue?: number | string | boolean;
  /** 静态值true，默认 是 */
  trueText?: string;
  /** 静态值false，默认 否 */
  falseText?: string;
}
const Switch = ({
  onChange,
  value,
  disabled,
  staticed,
  onBlur,
  compProps,
  trueValue = true,
  falseValue = false,
  trueText = '是',
  falseText = '否',
}: SwitchProps) => {
  if (staticed || disabled) {
    return <View className='pd-switch--static'>{typeof value === 'boolean' ? (value ? trueText : falseText) : ''}</View>;
  }
  let checked = false;
  if (typeof value === 'boolean' || typeof value === 'string' || typeof value === 'number') {
    checked = value === trueValue;
  }
  return (
    <SwitchComp
      disabled={disabled}
      onChange={e => {
        onChange?.(e.detail.value ? trueValue : falseValue);
      }}
      checked={checked}
      color={theme.primaryColor}
      {...compProps}
    />
  );
};
export default Switch;
