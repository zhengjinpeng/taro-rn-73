import theme from '@/theme';
import { Switch } from 'react-native';

const SwitchComp = ({ checked, onChange, disabled, color = theme.primaryColor }) => {
  return (
    <Switch
      trackColor={{ false: 'rgba(0,0,0,0.05)', true: 'rgba(0,0,0,0.05)' }}
      thumbColor={checked ? color : '#fff'}
      ios_backgroundColor='#3e3e3e'
      disabled={disabled}
      onValueChange={v => {
        if (disabled) return;
        onChange({
          detail: {
            value: v,
          },
        });
      }}
      value={checked}
    />
  );
};

export default SwitchComp;
