import theme from '@/theme';
import classNames from 'classnames';
import { memo, useState } from 'react';
import { Text, View } from '@tarojs/components';
import { Icon } from 'pd-taro-ui';
import './index.scss';
import type { SelectProps } from './index';
import { pxTransform } from '@tarojs/taro';

interface SelectItemProps {
  item: any;
  labelField: string;
  valueField: string;
  selectMode: SelectProps['selectMode'];
  getIsCheck: (item: any) => boolean;
  toggleCheck: (item: any) => any;
  expandKeys: any[];
  onToggleExpandItem: (v: string, item: any, isExpand: boolean) => any;
  deepth: number;
  disabled?: boolean;
  disabledCheck?: boolean;
  disabledExpand?: boolean;
  defer?: boolean;
  getIsLoading?: (item: any) => boolean;
}
const SelectItem = memo(
  ({
    selectMode,
    getIsCheck,
    item,
    labelField,
    valueField,
    toggleCheck,
    expandKeys,
    onToggleExpandItem,
    deepth,
    disabled = false,
    disabledCheck = false,
    disabledExpand = false,
    defer = false,
    getIsLoading,
  }: SelectItemProps) => {
    if (disabled) {
      disabledCheck = true;
      disabledExpand = true;
    }
    const hadValue = !!item[valueField] || typeof item[valueField] === 'number';
    if (!defer && !hadValue) disabledCheck = true;
    const isTreeMode = selectMode === 'tree';
    const checked = getIsCheck(item);
    const hadChildren = !!item.children?.length;
    const loading = !!getIsLoading?.(item);
    const isExpand = expandKeys.includes(item[valueField]);
    let expandBtnVisble = true;
    if (disabledExpand) {
      expandBtnVisble = false;
    } else if (!isTreeMode) {
      expandBtnVisble = false;
    } else {
      if (defer) {
        expandBtnVisble = true;
      } else {
        expandBtnVisble = hadChildren;
      }
    }
    return (
      <>
        <View
          className={classNames({
            'pd-select__item': true,
            'pd-select__item--checked': checked,
            'pd-select__item--disabled': disabled,
          })}
          onClick={e => {
            e?.stopPropagation?.();
            console.log(item, { expandBtnVisble, defer, isExpand, isTreeMode, hadChildren });
            if (disabledCheck) return;
            toggleCheck(item);
          }}
          hoverStyle={{
            opacity: 0.7,
          }}
          style={{
            marginLeft: pxTransform(deepth * 30),
          }}>
          {loading ? (
            <Icon name='change' size={40} color={theme.primaryColor} />
          ) : (
            <>
              {expandBtnVisble && (
                <Icon
                  size={40}
                  name={isExpand ? 'right' : 'down'}
                  color='#aaa'
                  onClick={e => {
                    e.stopPropagation?.();
                    onToggleExpandItem(item[valueField], item, !isExpand);
                  }}
                />
              )}
            </>
          )}
          <Text className={classNames('pd-select__item__text', { 'pd-select__item__text--disabled': disabled || disabledCheck })}>
            {item[labelField]}
          </Text>
          {checked && <Icon size={40} name='selected' color={theme.primaryColor} />}
        </View>
        {hadChildren &&
          isExpand &&
          item.children.map((cItem: any, cIndex) => {
            return (
              <SelectItem
                key={deepth + '_' + cIndex}
                selectMode={selectMode}
                item={cItem}
                labelField={labelField}
                valueField={valueField}
                toggleCheck={toggleCheck}
                getIsCheck={getIsCheck}
                expandKeys={expandKeys}
                onToggleExpandItem={onToggleExpandItem}
                deepth={deepth + 1}
                disabled={cItem.disabled}
                disabledCheck={cItem.disabledCheck}
                disabledExpand={cItem.disabledExpand}
                defer={defer}
                getIsLoading={getIsLoading}
              />
            );
          })}
      </>
    );
  },
);
export default SelectItem;
