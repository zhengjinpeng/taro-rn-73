import { createContext } from 'react';
import type { FormProps } from './index';

export const FormConfigContext = createContext<FormProps>({});
