import { View, PickerView, PickerViewColumn, Text } from '@tarojs/components';
import theme from '@/theme';
import { ComponentProps, useEffect, useMemo, useState } from 'react';
import './index.scss';
import classNames from 'classnames';
import { Calendar, Icon, PullView, Button } from 'pd-taro-ui';
import { useReactive } from 'ahooks';
import { pxTransform } from '@tarojs/taro';
import { transSelectedValueToValue, transValueToSelectedIndexes, transValueToSelectedValue } from '../Select/utils';
import { OptionItem } from './type';
import { isEqual } from 'lodash';

export interface PickerProps {
  /** selector单选，multiSelector多选。 默认 selector */
  mode?: 'selector' | 'multiSelector';
  options: OptionItem[] | OptionItem[][];
  value?: string | string[];
  onChange?: (value: PickerProps['value'], options: PickerProps['options']) => any;
  onBlur?: any;
  placeholder?: string;
  staticed?: boolean;
  disabled?: boolean;
  compProps?: any;
  className?: string;
  /** 是否显示清除按钮 */
  clearable?: boolean;
  /** 是否拼接字符串，返回数组或字符串，默认true字符串 */
  joinValues?: boolean;
  /** 字符串拼接符号，默认, */
  delimiter?: string;
  /** 渲染标签：默认label */
  labelField?: string;
  /** 渲染值：默认value */
  valueField?: string;
  /** 是否由外部传入静态展示值，默认false */
  isUseParentStatic?: boolean;
  /** 外部传入静态展示值 */
  staticStr?: string;
  /** picker标题：默认 请选择 */
  pickerTitle?: string;
  /** 选项变化时触发: preIndexs上次选中下标列表，indexs当前选中下标列表  */
  onColumnChange?: (e: {
    detail: { preIndexs: number[]; indexs: number[]; value: PickerProps['value']; options: PickerProps['options'] };
  }) => any;
}
const Picker = ({
  onChange,
  value,
  disabled,
  staticed,
  onBlur,
  clearable = false,
  placeholder = '请选择',
  className,
  compProps,
  mode = 'selector',
  options = [],
  joinValues = true,
  delimiter = ',',
  labelField = 'label',
  valueField = 'value',
  isUseParentStatic = false,
  pickerTitle = '请选择',
  onColumnChange,
  ...props
}: PickerProps) => {
  const state = useReactive({
    show: false,
    visible: false,
    /** 选中index列表 */
    selectedIndexs: [] as number[],
  });

  const optionList = useMemo(() => {
    if (mode === 'selector') {
      return [options as OptionItem[]];
    } else {
      return options as OptionItem[][];
    }
  }, [mode, options]);

  useEffect(() => {
    state.selectedIndexs.forEach((item, index) => {
      if (!optionList[index][item]) {
        state.selectedIndexs[index] = optionList[index].length - 1;
      }
    });
  }, [optionList]);

  const hadValue = useMemo(() => {
    let bool = false;
    if (isUseParentStatic) {
      bool = !!props.staticStr;
    } else {
      bool = typeof value === 'number' || !!value?.length;
    }
    return bool;
  }, [isUseParentStatic, props.staticStr, value]);

  const open = () => {
    if (disabled) return;
    state.show = true;
    if (!state.selectedIndexs.length) {
      state.selectedIndexs = optionList.map(_v => 0);
    }
    setTimeout(() => {
      state.visible = true;
    }, 300);
  };
  const close = () => {
    state.show = false;
    state.visible = false;
  };

  const getSubmitValue = () => {
    if (mode === 'selector') {
      const item = optionList[0][state.selectedIndexs[0]] || {};
      return [item[valueField] || '', [item]];
    } else {
      const items = state.selectedIndexs.length
        ? optionList.map((list, index) => {
            const item = list[state.selectedIndexs[index]];
            return item;
          })
        : [];
      console.log('items', items);
      return [
        transSelectedValueToValue(
          items.map(v => v[valueField]),
          true,
          joinValues,
          delimiter,
        ),
        items,
      ];
    }
  };
  const submit = () => {
    // console.log('submit ', state.selectedIndexs.concat([]), optionList);
    // @ts-ignore
    onChange?.(...getSubmitValue());
    close();
  };
  const clear = () => {
    state.selectedIndexs = [];
    submit();
  };

  const staticStr = useMemo(() => {
    if (isUseParentStatic) {
      return props.staticStr;
    }
    let values: string[] = [];
    let str = '';
    if (!value?.length) return str;
    if (mode === 'selector') {
      values = [value as string];
    } else if (mode === 'multiSelector') {
      if (joinValues) {
        values = (value as string).split(delimiter);
      } else {
        values = value as string[];
      }
    }
    str = values
      .map((v, i) => {
        return optionList[i].find(item => item[valueField] === v)?.[labelField] || '';
      })
      .join(delimiter);
    return str;
  }, [isUseParentStatic, value, mode, delimiter, props.staticStr, joinValues, optionList, labelField, valueField]);

  useEffect(() => {
    if (!state.show) {
      const multiple = mode == 'multiSelector';
      const selectedIndexs = transValueToSelectedIndexes(value, optionList, multiple, joinValues, delimiter, valueField);
      if (!isEqual(selectedIndexs, state.selectedIndexs.concat())) {
        state.selectedIndexs = selectedIndexs;
      }
    }
  }, [delimiter, joinValues, mode, optionList, state, state.show, value, valueField]);

  if (staticed) {
    return <View className='pd-picker--static'>{staticStr}</View>;
  }
  return (
    <>
      <View className={classNames({ 'pd-picker': true, 'pd-picker--disabled': disabled })}>
        <View className={classNames({ 'pd-picker__content': true, 'pd-picker__placeholder': !hadValue })} onClick={open}>
          {staticStr || placeholder || ' '}
        </View>
        {clearable && !disabled && hadValue && <Icon name='error' color='#bbb' size={36} onClick={clear} />}
        <Icon name='right' color='#bbb' size={36} />
      </View>

      <PullView open={state.show} onClose={close} side='bottom'>
        <View className='pd-picker__modal'>
          <View className='pd-picker__modal__header'>{pickerTitle}</View>
          <View className='pd-picker__modal__close' onClick={close}>
            <Icon name='close' size={46} />
          </View>

          <View className='pd-picker__modal__pickerwrap'>
            {state.visible && (
              <PickerView
                indicatorStyle={{ height: pxTransform(45) } as any}
                // indicatorStyle='height: 45px;'
                className='pd-picker__modal__picker'
                value={state.selectedIndexs.concat()}
                onChange={e => {
                  if (isEqual(state.selectedIndexs.concat(), e.detail.value)) return;
                  console.log('Picker onChange ', state.selectedIndexs.concat(), e.detail.value.concat());
                  const columnChangeEvent = {
                    detail: {
                      preIndexs: e.detail.value.concat(),
                      indexs: state.selectedIndexs.concat(),
                      value: '',
                      options: [],
                    },
                  };
                  state.selectedIndexs = e.detail.value;
                  columnChangeEvent.detail.value = getSubmitValue()[0];
                  columnChangeEvent.detail.options = getSubmitValue()[1];
                  onColumnChange?.(columnChangeEvent);
                }}>
                {optionList.map((option, optionIndex) => {
                  return (
                    <PickerViewColumn key={optionIndex}>
                      {option.map((item, index) => {
                        return (
                          <View key={index} className='pd-picker__modal__item'>
                            {item[labelField]}
                          </View>
                        );
                      })}
                    </PickerViewColumn>
                  );
                })}
              </PickerView>
            )}
          </View>

          <Button type='primary' style={{ width: pxTransform(300), alignSelf: 'center', marginTop: pxTransform(30) }} onClick={submit}>
            确定
          </Button>
        </View>
      </PullView>
    </>
  );
};
export default Picker;
