export interface OptionItem {
  label?: string;
  value?: string;
  [key: string]: any;
}

/**
 * 选择范围：默认 day年月日
 *
 * day年月日, year年, month年月, day-time年月日时分秒, day-time-minute年月日时分, time时分秒, hour-minute时分, hour小时, minute分, minute-second分秒, second秒 */
export type TimeRange =
  | 'day'
  | 'year'
  | 'month'
  | 'day-time'
  | 'day-hour-minute'
  | 'time'
  | 'hour-minute'
  | 'hour'
  | 'minute'
  | 'minute-second'
  | 'second';
