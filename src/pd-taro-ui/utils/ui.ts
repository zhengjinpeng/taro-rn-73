import { getSystemInfoSync } from '@tarojs/taro';

export const getStatusBarHeight = () => {
  return getSystemInfoSync().statusBarHeight || 0;
};
