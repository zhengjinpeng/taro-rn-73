import { createContext, useContext } from 'react';

const context = createContext({ id: null });

const CustomWrapper = ({ children }) => {
  return children;
};

CustomWrapper.useContext = () => useContext(context);
export default CustomWrapper;
