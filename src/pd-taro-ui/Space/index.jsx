import Taro from '@tarojs/taro';
import classNames from 'classnames';
import View from '../View';
import './index.scss';

const Space = ({ children, gap = 24, between, row, wrap, justify, items, grow, shrink, self, style, className, ...props }) => {
  const _gap = Taro.pxTransform(gap);

  return (
    <View
      className={classNames(
        justify ? 'justify-' + justify : '',
        items ? 'items-' + items : '',
        row && 'flex-row',
        wrap && 'flex-wrap',
        grow && 'w-0 flex-grow',
        shrink && 'flex-shrink',
        self && 'self-' + self,
        className,
      )}
      style={{ ...style, gap: _gap }}
      {...props}>
      {children}
    </View>
  );
};
export default Space;
