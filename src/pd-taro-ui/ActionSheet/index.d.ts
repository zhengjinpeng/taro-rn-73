import { LegacyRef, ComponentType } from 'react';

interface ActionSheetProps {
  value;
  range;
  /** default 'name' */
  nameKey;
  /** default 'value' */
  valueKey;
  onChange;
  hildren;
  /** 子元素 */
  children?: any;
}

export const ActionSheet: ComponentType<ActionSheetProps>;
export default ActionSheet;
