import { View, Text, Image } from '@tarojs/components';
import Taro, { useDidShow, useDidHide } from '@tarojs/taro';
import classNames from 'classnames';
import React, { useMemo, useState, useCallback, createContext, useContext, useEffect, useRef } from 'react';
import Badge from '../Badge';
import './index.scss';
import Icon, { IconProps } from '../Icon';

interface TabItem {
  key: string;
  name: string;
  icon?: IconProps['name'];
  focusIcon?: IconProps['name'];
  imageUrl?: string;
  focusImageUrl?: string;
  /** 未读数量 */
  num: number;
  /** 自定义点击事件 */
  onClick?: (tabItem: TabItem) => void;
}

export interface TabBarProps {
  focusKey: string;
  tabs: TabItem[];
}
const TabBar = ({ tabs, focusKey }: TabBarProps) => {
  const onItemClick = (item: TabItem) => {
    item.onClick?.(item);
  };
  return (
    <View className='pd-tabBar'>
      {tabs.map(item => {
        const isFocus = item.key === focusKey;
        return (
          <View
            className={classNames({
              'pd-tabBar__item': true,
              'pd-tabBar__item--focus': isFocus,
            })}
            key={item.key}
            onClick={() => onItemClick(item)}>
            <Icon name={(isFocus ? item.focusIcon : item.icon) || item.icon} color='grey' />
            <Text
              className={classNames({
                'pd-tabBar__item__name': true,
                'pd-tabBar__item__name--focus': isFocus,
              })}>
              {item.name}
            </Text>
            {!!item.num && <Badge count={item.num} />}
          </View>
        );
      })}
    </View>
  );
};
export default TabBar;
