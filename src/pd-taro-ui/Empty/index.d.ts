import { ReactNode } from 'react';

interface EmptyProps {
  /** 标题 */
  title?: string;
  /** 图片链接 */
  url?: string;
  /** icon name */
  icon?: ReactNode;
  /** 底部自定义渲染 */
  renderFooter?: ReactNode;
  /** 自定义样式名 */
  className?: string;
  /** 自定义样式 */
  style?: string;
}

const Empty: React.FC<EmptyProps>;

export default Empty;
