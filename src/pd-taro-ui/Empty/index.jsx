import { Image, View } from '@tarojs/components';
import classNames from 'classnames';
import empry from './images/empty.png';
import Text from '../Text';
import './index.scss';
import Icon from 'pd-taro-ui/Icon';

const Empty = ({ url, icon, title = '暂无数据', renderFooter, className, style, ...props }) => {
  return (
    <View className={classNames('pd-empty', 'items-center', className)} style={style} {...props}>
      {icon || <Icon name='empty' size={200} color='#ccc' />}
      <Text className='pd-empty__title' align='center'>
        {title}
      </Text>
      {renderFooter}
    </View>
  );
};
export default Empty;
