import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import classNames from 'classnames';
import theme from '@/theme';
import Text from '../Text';
import BoxShadow from '../BoxShadow';
import './index.scss';

const Card = ({
  shadow = theme.card.shadow,
  radius = theme.card.radius,
  verticalPadding = true,
  margin,
  disableMarginBottom,
  disableMarginTop,
  children,
  className,
  row,
  wrap,
  justify,
  items,
  style,
  ...props
}) => {
  const cn = classNames(
    'pd-card',
    !verticalPadding && 'pd-card--v-p',
    margin && 'pd-card--margin',
    disableMarginBottom && 'pd-card--margin-bottom',
    disableMarginTop && 'pd-card--margin-top',
    justify ? 'justify-' + justify : '',
    items ? 'items-' + items : '',
    row && 'flex-row',
    wrap && 'flex-wrap',
    className,
  );

  if (shadow) {
    return (
      <BoxShadow className={cn} radius={radius} style={style} opacity={0.3} {...props}>
        {children}
      </BoxShadow>
    );
  } else {
    return (
      <View className={cn} style={{ ...style, borderRadius: Taro.pxTransform(radius) }} {...props}>
        {children}
      </View>
    );
  }
};

const Title = ({ numberOfLines = 1, line = true, children, sizeStyle = {} }) => {
  return (
    <View className='pd-cardTitle'>
      {line && <View className='pd-cardTitle__line' />}
      <Text className='pd-cardTitle__text' style={sizeStyle} numberOfLines={numberOfLines}>
        {children}
      </Text>
    </View>
  );
};

Card.Title = Title;
export default Card;
