import classNames from 'classnames';
import { memo } from 'react';
import View from '../View';

const Column = memo(({ justify, items, grow, shrink, self, className, style, ...props }) => {
  return (
    <View
      className={classNames(
        justify && 'justify-' + justify,
        items && 'items-' + items,
        grow && 'flex-grow',
        shrink && 'flex-shrink',
        self && 'self-' + self,
        className,
      )}
      style={style}
      {...props}
    />
  );
});
export default Column;
