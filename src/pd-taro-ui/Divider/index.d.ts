import { View, ViewProps } from '@tarojs/components';

interface GroupProps {
  /**
   * 线的粗细
   * @default 1
   */
  size?: number;
  /**
   * 线的样式，支持'dotted','dashed','solid'三种
   * @default 'solid'
   */
  type?: 'dotted' | 'dashed' | 'solid';
  /**
   * 两端的内边距
   * @default 0
   */
  padding?: number;
  children?: any;
}

export interface DividerProps {
  /**
   * 分割线的方向
   * @default 'horizontal'
   */
  direction?: 'horizontal' | 'vertical';
  /**
   * 线的粗细
   * @default 1
   */
  size?: number;
  /**
   * 线的样式，支持'dotted','dashed','solid'三种
   * @default 'solid'
   */
  type?: 'dotted' | 'dashed' | 'solid';
  /**
   * 两端的内边距
   * @default 0
   */
  padding?: number;
  children?: any;
}

/**
 * 分割线组件
 */
declare const Divider: {
  (props: DividerProps): JSX.Element;
  Group: (props: GroupProps) => any;
};
export default Divider;
