import { ComponentType } from 'react';

interface MapProps {
  onMoveEnd;
  center;
  zoom: number;
  markers;
  circles;
  polylines;
  polygons;
  onMarkerClick;
}

export const Map: ComponentType<MapProps>;
