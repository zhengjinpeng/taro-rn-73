import { ReactNode } from 'react';

interface StepProps {
  // 列表数据
  data;
  // row 横向 column 纵向
  type: 'row' | 'column';
  // 横向时上面的渲染内容 纵向是左侧的渲染内容
  renderStart;
  // 指定尺寸 横向时为高度 纵向时为宽度 不指定则不会渲染开始块
  startSize;
  // 横向时下面的渲染内容 纵向是右侧的渲染内容
  renderEnd;
  // 渲染中间的点的内容 获取在data的每一项上传入pointColor会自动渲染颜色
  renderPoint;
  // 当为纵向是设置点距离顶部的距离
  pointTop: number;
  //
  onItemClick;
  className;
  style;
  itemClassName;
  itemStyle;
}

/**
 * 横向时点在中间 纵向是点在距离顶部pointTop距离处
 * @param {*} param0
 * @returns
 */
const Step: React.FC<StepProps>;
export default Step;
