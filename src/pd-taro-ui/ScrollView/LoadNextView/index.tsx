import { View } from '@tarojs/components';
import classNames from 'classnames';
import './index.scss';
import { memo } from 'react';

interface LoadNextViewProps {
  visible: boolean;
  loading: boolean;
  loadingText?: string;
  className?: string;
  children?: any;
}

const LoadNextView = memo(({ children, className, loading, visible, loadingText = '加载中...' }: LoadNextViewProps) => {
  if (!visible) return;
  return <View className={classNames('LoadNextView', className)}>{children ? children : loading && loadingText}</View>;
});

export default LoadNextView;
