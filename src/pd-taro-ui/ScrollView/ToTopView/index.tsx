import classNames from 'classnames';
import './index.scss';
import { BoxShadow, Icon } from 'pd-taro-ui';
import { memo } from 'react';

interface ToTopViewProps {
  visible: boolean;
  className?: string;
  children?: any;
  onClick?: any;
}

const ToTopView = memo(({ visible, className, onClick }: ToTopViewProps) => {
  if (!visible) return;
  return (
    <BoxShadow className={classNames('ToTopView', className)} radius={30} onClick={onClick}>
      <Icon name='up' size={24} />
    </BoxShadow>
  );
});

export default ToTopView;
