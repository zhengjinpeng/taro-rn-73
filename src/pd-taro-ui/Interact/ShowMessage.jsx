import { Text } from '@tarojs/components';
import { useEffect, useRef } from 'react';
import BoxShadow from '../BoxShadow';
import './ShowMessage.scss';
import Taro from '@tarojs/taro';
import { navigateTo } from '@/utils/router';

export const ShowMessage = ({ url, title, content, onTopViewRemove }) => {
  const { statusBarHeight = 0 } = Taro.getSystemInfoSync();

  const timer = useRef(null);

  useEffect(() => {}, []);

  useEffect(() => {
    if (timer.current) {
      clearTimeout(timer.current);
    }
    timer.current = setTimeout(() => {
      onTopViewRemove?.();
    }, 3000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [title, content, url]);

  return (
    <BoxShadow opacity={0.2} onClick={() => navigateTo(url)} className='pd-showMessage' radius={16} style={{ top: statusBarHeight + 10 }}>
      <Text className='pd-showMessage__title'>{title}</Text>
      {!!content && <Text className='pd-showMessage__content'>{content}</Text>}
    </BoxShadow>
  );
};
