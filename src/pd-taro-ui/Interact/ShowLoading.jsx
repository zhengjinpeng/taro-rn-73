import { View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import Loading from '../Loading';

import './ShowLoading.scss';

export const ShowLoading = ({ text = '请稍后', mask }) => {
  return (
    <>
      {mask && <View className='pd-showLoading__mask' />}
      <View className='pd-showLoading'>
        <Loading color='blank' size={64} />
        <Text className='pd-showLoading__text'>{text}</Text>
      </View>
    </>
  );
};
