export interface CalendarRenderItem {
  text: string;
  value: string; //'YYYY-MM-DD'
  disabled: boolean;
  active: boolean;
  activeFirst: boolean;
  activeLast: boolean;
}
