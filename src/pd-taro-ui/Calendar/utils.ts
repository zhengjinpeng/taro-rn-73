import dayjs, { Dayjs } from 'dayjs';

export function isInTimeRange(date: string | Dayjs, startDay: string | Dayjs, endDay: string | Dayjs) {
  const data = {
    active: false,
    activeFirst: false,
    activeEnd: false,
  };
  if (!date || !startDay || !endDay) {
    return data;
  }
  const time = dayjs(date).startOf('day');
  const startDate = dayjs(startDay).startOf('day');
  const endDate = dayjs(endDay).startOf('day');

  if (startDate.valueOf() <= time.valueOf() && endDate.startOf('day').valueOf() >= time.valueOf()) {
    data.active = true;
    if (startDate.valueOf() === time.valueOf()) {
      data.activeFirst = true;
    }
    if (endDate.valueOf() === time.valueOf()) {
      data.activeEnd = true;
    }
  }
  //   console.log('isInTimeRange', time.format('YYYY-MM-DD'), [startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD')], data);
  return data;
}
