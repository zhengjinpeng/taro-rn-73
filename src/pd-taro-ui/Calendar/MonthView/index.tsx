import { Text, View } from '@tarojs/components';
import theme from '@/theme';
import { ComponentProps, useMemo, useState } from 'react';
import '../index.scss';
import classNames from 'classnames';
import { Grid, Icon } from 'pd-taro-ui';
import { pxTransform } from '@tarojs/taro';
import dayjs, { Dayjs } from 'dayjs';
import { isInTimeRange } from '../utils';
import CalendarItem from '../CalendarItem';
import type { CalendarRenderItem } from '../type';
import { useReactive } from 'ahooks';

interface MonthViewProps {
  placeholder?: string;
  /** day单选， range日期范围(start,end) ,  默认day */
  mode?: 'day' | 'range';
  value?: string | string[];
  onChange?: (value: MonthViewProps['value']) => void;
  onBlur?: any;
  staticed?: boolean;
  disabled?: boolean;
  clearable?: boolean;
  className?: string;
  // minDate?: string;
  // maxDate?: string;
  // disabledDate?: (date: Dayjs) => boolean;
}

const MonthView = ({
  onChange,
  value,
  disabled,
  staticed,
  onBlur,
  clearable = false,
  placeholder,
  className,
  // disabledDate,
  // minDate,
  // maxDate,
  mode = 'day',
}: MonthViewProps) => {
  const values = useMemo(() => {
    if (!value) {
      return [];
    }
    if (Array.isArray(value)) {
      return value;
    }
    return [value];
  }, [value]);
  /** 当前选中的年月 */
  const [yearMonth, setYearMonth] = useState(dayjs().startOf('year'));

  const state = useReactive({
    /** 临时用，给range mode使用 */
    tempRange: [] as string[],
  });
  // console.log('tempRange', tempRange);
  // console.log('values', values);

  const prevBig = () => {
    setYearMonth(yearMonth.subtract(1, 'year'));
  };

  const nextBig = () => {
    setYearMonth(yearMonth.add(1, 'year'));
  };

  const items = useMemo(() => {
    const list: CalendarRenderItem[] = [];
    // 1-12
    for (let i = 0; i < 12; i++) {
      let itemDisabled = false;
      let itemActive = false;
      let itemActiveFirst = false;
      let itemActiveLast = false;
      const value0 = values[0];
      const value1 = values[1];
      const date = yearMonth.add(i, 'month');
      if (mode === 'day') {
        if (value0 && dayjs(value0).format('YYYY-MM') === date.format('YYYY-MM')) {
          itemActive = true;
        }
      } else if (mode == 'range') {
        if (state.tempRange.length) {
          if (state.tempRange.length == 1) {
            if (state.tempRange[0] && dayjs(state.tempRange[0]).format('YYYY-MM') === date.format('YYYY-MM')) {
              itemActive = true;
              itemActiveFirst = true;
            }
          } else {
            const data = isInTimeRange(date, state.tempRange[0], state.tempRange[1]);
            itemActive = data.active;
            itemActiveFirst = data.activeFirst;
            itemActiveLast = data.activeEnd;
          }
        } else {
          const data = isInTimeRange(date, value0, value1);
          itemActive = data.active;
          itemActiveFirst = data.activeFirst;
          itemActiveLast = data.activeEnd;
        }
      }
      list.push({
        text: date.format('MM') + '月',
        value: date.format('YYYY-MM'),
        disabled: itemDisabled,
        active: itemActive,
        activeFirst: itemActiveFirst,
        activeLast: itemActiveLast,
      });
    }
    // console.log('yearView list', list);
    return list;
  }, [mode, state.tempRange, values, yearMonth]);

  const onClickItem = (item: CalendarRenderItem) => {
    if (mode === 'day') {
      onChange?.(item.value);
    } else if (mode === 'range') {
      const { tempRange } = state;
      console.log('onClickItem', { item, mode, tempRange });
      if (!state.tempRange.length) {
        state.tempRange = [item.value];
      } else if (tempRange.length === 1) {
        let newList: string[] = [];
        const pre = dayjs(tempRange[0]);
        if (pre.startOf('day').valueOf() === dayjs(item.value).startOf('day').valueOf()) {
          newList = [item.value, item.value];
        } else {
          if (pre.startOf('day').valueOf() > dayjs(item.value).startOf('day').valueOf()) {
            newList = [item.value, tempRange[0]];
          } else {
            newList = [tempRange[0], item.value];
          }
        }
        state.tempRange = newList;
        onChange?.(newList);
      } else if (tempRange.length === 2) {
        state.tempRange = [];
        onChange?.([]);
      }
    }
  };

  const headerText = useMemo(() => {
    return `${yearMonth.format('YYYY')}年`;
  }, [yearMonth]);

  if (staticed) {
    return <View className='pd-calendar--static'>{value}</View>;
  }
  return (
    <View className={classNames({ 'pd-calendar': true, 'pd-calendar--disabled': disabled }, className)}>
      <View className='pd-calendar__head'>
        <Icon name='left-double-arrow' className='pd-calendar__head__icon' onClick={prevBig} style={{ marginRight: pxTransform(10) }} />
        <Text className='pd-calendar__head__text'>{headerText}</Text>
        <Icon name='right-double-arrow' className='pd-calendar__head__icon' onClick={nextBig} style={{ marginLeft: pxTransform(10) }} />
      </View>
      <View className='pd-calendar__list'>
        <Grid column={3}>
          {items.map(item => (
            <CalendarItem
              key={'day_' + item}
              disabled={item.disabled}
              active={item.active}
              activeFirst={item.activeFirst}
              activeLast={item.activeLast}
              onClick={() => onClickItem(item)}>
              {item.text}
            </CalendarItem>
          ))}
        </Grid>
      </View>
    </View>
  );
};

export default MonthView;
