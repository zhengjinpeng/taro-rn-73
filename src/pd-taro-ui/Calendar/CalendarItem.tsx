import classNames from 'classnames';
import { useState } from 'react';
import Column from '../Column';
import './index.scss';
import { Text } from '@tarojs/components';

interface CalendarItemProps {
  children?: any;
  onClick?: any;
  disabled: boolean;
  disabledColor?: boolean;
  active: boolean;
  activeFirst: boolean;
  activeLast: boolean;
}
const CalendarItem = ({ children, disabled, onClick, active, activeFirst, activeLast, disabledColor }: CalendarItemProps) => {
  const [hover, setHover] = useState(false);
  const allSelect = activeFirst && activeLast;
  if (allSelect) {
    activeFirst = false;
    activeLast = false;
  }
  return (
    <Column
      className={classNames('pd-calendar__item', {
        'pd-calendar__item-hover': hover,
        'pd-calendar__item-disabled': disabledColor,
        'pd-calendar__item-active': active,
        'pd-calendar__item-activeFirst': activeFirst,
        'pd-calendar__item-activeLast': activeLast,
        'pd-calendar__item-allSelect': allSelect,
      })}
      justify='center'
      items='center'
      onClick={() => !disabled && onClick()}
      onTouchStart={() => !disabled && setHover(true)}
      onTouchEnd={() => setHover(false)}
      onTouchMove={() => setHover(false)}>
      {(activeFirst || allSelect) && <Text className={classNames('pd-calendar__item__text', 'pd-calendar__item__text-first')}>开始</Text>}
      {children}
      {(activeLast || allSelect) && <Text className={classNames('pd-calendar__item__text', 'pd-calendar__item__text-end')}>结束</Text>}
    </Column>
  );
};
export default CalendarItem;
