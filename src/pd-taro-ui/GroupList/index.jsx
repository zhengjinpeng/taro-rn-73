import { View } from '@tarojs/components';
import Space from '../Space';
import './index.scss';

const GroupListItem = ({ title, desc = '', children }) => {
  return (
    <>
      <View className='pd-groupList__title'>{title}</View>
      {children}
      <View className='pd-groupList__desc'>{desc}</View>
    </>
  );
};

const GroupList = ({ children }) => {
  return <Space className='pd-groupList'>{children}</Space>;
};

GroupList.Item = GroupListItem;
export default GroupList;
