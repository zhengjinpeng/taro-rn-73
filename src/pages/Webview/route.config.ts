export type PageParams = {
  url: string;
  title?: string;
};

export type PageData = {};

export type PageBackData = {};

export const middlewareConfig = {};
