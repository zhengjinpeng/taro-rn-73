import { useRouterData } from '@/utils/router';
import { View, WebView } from '@tarojs/components';
import { useEffect } from 'react';
import { PageParams } from './route.config';
import Taro from '@tarojs/taro';
import { Header, TopView } from 'pd-taro-ui';

const FilePreview = () => {
  const router = useRouterData();
  const pageParams = router.params as PageParams;

  useEffect(() => {
    pageParams.title && Taro.setNavigationBarTitle({ title: pageParams.title });
  });
  /*  #ifdef  rn  */
  return (
    <TopView>
      <Header title={pageParams.title || 'webview'} titleCenter />
      <WebView src={pageParams.url} onError={e => console.log(e)} />
    </TopView>
  );
  /*  #endif  */
  /*  #ifndef  rn  */
  return <WebView src={pageParams.url} onError={e => console.log(e)} />;
  /*  #endif  */
};
export default FilePreview;
