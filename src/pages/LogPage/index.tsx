import { Header, ScrollView, Cell, GroupList, TopView, Button } from 'pd-taro-ui';
import { copyLogFileToDownload, getLogFilePath, getLogText } from '@/utils/appLog';
import { Text, View } from '@tarojs/components';
import { useEffect, useState } from 'react';
import Taro from '@tarojs/taro';

const LogPage = () => {
  const [logStr, setlogStr] = useState('');
  const [filePath, setFilePath] = useState('');
  const [testObj, settestObj] = useState<any>({ a: '' });

  const readLog = () => {
    getLogText().then(res => {
      setlogStr(res);
    });
  };

  useEffect(() => {
    readLog();
    setFilePath(getLogFilePath());
  }, []);
  return (
    <TopView>
      <Header title='日志' titleCenter />
      <ScrollView>
        <View>
          <Text selectable>错误日志：{filePath}</Text>
        </View>
        <Button
          onClick={() => {
            throw new Error('测试错误');
          }}>
          抛出错误
        </Button>
        <Button
          onClick={() => {
            settestObj(null);
          }}>
          抛出界面错误
        </Button>
        <Text>{testObj.a}</Text>
        <Button onClick={readLog}>刷新日志</Button>
        <Button
          onClick={async () => {
            const downloadPath = await copyLogFileToDownload();
            Taro.showModal({
              title: '日志已下载到',
              content: downloadPath,
              showCancel: false,
            });
          }}>
          下载日志
        </Button>
        <Text selectable>{logStr}</Text>
      </ScrollView>
    </TopView>
  );
};
export default LogPage;
