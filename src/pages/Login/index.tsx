import Taro, { useDidHide, useDidShow, useLoad, useRouter, useUnload } from '@tarojs/taro';
import { Image, Input, Text, View } from '@tarojs/components';
import { Button, Cell, GroupList, Header, Icon, ScrollView, TopView } from 'pd-taro-ui';
import { PageParams, PageData, PageBackData } from './route.config';
import { navigateTo, redirectTo, reLaunch, useRouterData, navigateBack } from '@/utils/router';
import './index.scss';
import { useState } from 'react';
import useUserStore from '@/store/useUserStore';

const prefix = 'LoginPage__';

let clickCount = 0;
const LoginPage = () => {
  const router = useRouterData();
  const pageData = router.data as PageData;
  const pageParams = router.params as PageParams;
  const pageId = router.pageId;

  const [isUsePhone, setIsUsePhone] = useState(false);
  const [checked, setChecked] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [phoneCode, setPhoneCode] = useState('');

  const onConfirm = () => {
    let errMsg = '';
    if (!checked) {
      errMsg = '请阅读并同意用户协议';
    } else if (isUsePhone) {
      if (!username) {
        errMsg = '请输入用户名';
      } else if (!phoneCode) {
        errMsg = '请输入手机验证码';
      }
    } else if (!isUsePhone) {
      if (!username) {
        errMsg = '请输入用户名';
      } else if (!password) {
        errMsg = '请输入密码';
      }
    }
    if (errMsg) {
      Taro.showToast({
        title: errMsg,
        icon: 'none',
      });
      return;
    }
    // todo
    useUserStore.getState().doLogin({ username, password });
  };

  useLoad(() => {
    console.log('LoginPage onLoad');
  });

  return (
    <TopView>
      <Header title='登录' titleCenter />
      <ScrollView>
        <View className={prefix + 'content'}>
          <Image
            className={prefix + 'logo'}
            src={require('@/assets/logo.png')}
            onClick={() => {
              clickCount++;
              if (clickCount > 2) {
                navigateTo({ url: '/pages/LogPage/index' });
              }
            }}
          />
          <View className={prefix + 'label'}>APP</View>
          <View className={prefix + 'login-box'}>
            <View className={prefix + 'input-box'}>
              <Icon name='phone' size={46} />
              <Input className={prefix + 'input'} value={username} onInput={e => setUsername(e.detail.value)} placeholder='请输入用户名' />
            </View>
            <View className={prefix + 'input-box'}>
              <Icon name='security' size={46} />
              {isUsePhone ? (
                <Input
                  className={prefix + 'input'}
                  type='text'
                  value={phoneCode}
                  onInput={e => setPhoneCode(e.detail.value)}
                  placeholder='请输入验证码'
                />
              ) : (
                <Input
                  className={prefix + 'input'}
                  password={true}
                  value={password}
                  onInput={e => setPassword(e.detail.value)}
                  placeholder='请输入密码'
                  onConfirm={onConfirm}
                />
              )}
            </View>
            <View className={prefix + 'code-login-btn'} onClick={() => setIsUsePhone(!isUsePhone)}>
              用手机验证码登录
            </View>
            <Button type='primary' radiusType='round' className={prefix + 'submit-btn'} onClick={onConfirm}>
              立即登录
            </Button>
          </View>
          <View className={prefix + 'to_reset'}>
            <View
              className={prefix + 'to_reset_text'}
              onClick={() => {
                // todo
              }}>
              点击前往注册
            </View>
            <View className={prefix + 'to_reset_line'}></View>
            <View
              className={prefix + 'to_reset_text'}
              onClick={() => {
                // todo
              }}>
              忘记密码
            </View>
          </View>

          <View className={prefix + 'agree-box'}>
            <Icon
              name={checked ? 'Checked' : 'Unchecked'}
              size={50}
              color={checked ? '#007AFF' : '#cccccc'}
              onClick={() => setChecked(!checked)}></Icon>
            <Text className={prefix + 'agree-box-text'}>我已阅读与同意</Text>
            <Text
              className={prefix + 'agree-box-blue'}
              onClick={() => {
                // todo
              }}>
              《用户服务协议》
            </Text>
            <Text className={prefix + 'agree-box-text'}>和</Text>
            <Text
              className={prefix + 'agree-box-blue'}
              onClick={() => {
                // todo
              }}>
              《隐私政策》
            </Text>
          </View>
          <View className={prefix + 'bottom-tip'}>
            {process.env.TARO_APP_MODE !== 'prod' && `${process.env.TARO_APP_MODE}服`} v{process.env.TARO_APP_VERSION}
          </View>
        </View>
      </ScrollView>
    </TopView>
  );
};
export default LoginPage;
