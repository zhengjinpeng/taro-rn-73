import { Header, ScrollView, Cell, GroupList, TopView, Button } from 'pd-taro-ui';
import { navigateTo } from '@/utils/router';
import { previewFile } from '@/utils/platform';

const Index = () => {
  return (
    <TopView>
      <Header title='pd-taro-phone' titleCenter />
      <ScrollView>
        <GroupList>
          <GroupList.Item title='常用页面'>
            <Cell.Group>
              <Cell title='登录页' onClick={() => navigateTo({ url: '/pages/Login/index' })} />
              <Cell
                title='页面生命周期+路由'
                onClick={() => navigateTo({ url: '/example/Demo/BasePage/index', params: { testId: '1' } })}
              />
              <Cell title='formkit+全表单组件' onClick={() => navigateTo({ url: '/example/Demo/FormPage/index' })} />
              <Cell title='通用查询列表' onClick={() => navigateTo({ url: '/example/Demo/ListPage/index' })} />
              <Cell title='通用详情页面' onClick={() => navigateTo({ url: '/example/Demo/DetailPage/index' })} />
              <Cell title='通用详情tab页面' onClick={() => navigateTo({ url: '/example/Button' })} />
              {process.env.TARO_ENV === 'rn' && <Cell title='RN日志页' onClick={() => navigateTo({ url: '/pages/LogPage/index' })} />}
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='基础组件'>
            <Cell.Group>
              <Cell title='Button 按钮' onClick={() => navigateTo({ url: '/example/Button' })} />
              <Cell title='Cell 单元格' onClick={() => navigateTo({ url: '/example/Cell' })} />
              <Cell title='LinearGradient 渐变' onClick={() => navigateTo({ url: '/example/LinearGradient' })} />
              <Cell title='BoxShadow 阴影' onClick={() => navigateTo({ url: '/example/BoxShadow' })} />
              <Cell title='Loading 加载动画' onClick={() => navigateTo({ url: '/example/Loading' })} />
              <Cell title='Icon 图标' onClick={() => navigateTo({ url: '/example/Icon' })} />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='布局组件'>
            <Cell.Group>
              <Cell title='Column flex竖向' onClick={() => navigateTo({ url: '/example/Column' })} />
              <Cell title='Row flex横向' onClick={() => navigateTo({ url: '/example/Row' })} />
              <Cell title='Space 间距' onClick={() => navigateTo({ url: '/example/Space' })} />
              <Cell title='Divider 分割线' onClick={() => navigateTo({ url: '/example/Divider' })} />
              <Cell title='Grid 宫格' onClick={() => navigateTo({ url: '/example/Grid' })} />
              <Cell title='Card 卡片' onClick={() => navigateTo({ url: '/example/Card' })} />
              {/* <Cell title='ScrollView 滚动容器' onClick={() => navigateTo({ url: '/example/ScrollView'})} />
            <Cell title='ScrollViewHorizontal 横向滚动容器' onClick={() => navigateTo({ url: '/example/ScrollViewHorizontal'})} />
            <Cell title='ScrollViewManage 滚动容器管理器' onClick={() => navigateTo({ url: '/example/ScrollViewManage'})} /> */}
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='导航组件'>
            <Cell.Group>
              <Cell title='Header 头部导航' onClick={() => navigateTo({ url: '/example/Header' })} />
              <Cell title='Tab 选项卡切换' onClick={() => navigateTo({ url: '/example/Tab' })} />
              <Cell title='TabBar 底部导航' onClick={() => navigateTo({ url: '/example/TabBar' })} />
              <Cell title='Elevator 电梯楼层' onClick={() => navigateTo({ url: '/example/Elevator' })} />
              <Cell title='Menu 下拉菜单' onClick={() => navigateTo({ url: '/example/Menu' })} />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='表单组件'>
            <Cell.Group>
              <Cell title='Switch 开关' onClick={() => navigateTo({ url: '/example/Switch' })} />
              <Cell title='Calendar 日历' onClick={() => navigateTo({ url: '/example/Calendar' })} />
              <Cell title='Upload 上传' onClick={() => navigateTo({ url: '/example/Upload' })} />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='展示组件'>
            <Cell.Group>
              <Cell title='Text 文本' onClick={() => navigateTo({ url: '/example/Text' })} />
              <Cell title='Image 图片' onClick={() => navigateTo({ url: '/example/Image' })} />
              <Cell title='Badge 徽标' onClick={() => navigateTo({ url: '/example/Badge' })} />
              <Cell title='Tag 标签' onClick={() => navigateTo({ url: '/example/Tag' })} />
              <Cell title='Avatar 头像' onClick={() => navigateTo({ url: '/example/Avatar' })} />
              <Cell title='HtmlView 富文本' onClick={() => navigateTo({ url: '/example/HtmlView' })} />
              <Cell title='Step 步骤条' onClick={() => navigateTo({ url: '/example/Step' })} />
              <Cell title='Empty 空数据' onClick={() => navigateTo({ url: '/example/Empty' })} />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='操作反馈'>
            <Cell.Group>
              <Cell title='LongPress 长按' onClick={() => navigateTo({ url: '/example/LongPress' })} />
              <Cell title='Layout 布局计算' onClick={() => navigateTo({ url: '/example/Layout' })} />
              <Cell title='TopView 顶层容器' onClick={() => navigateTo({ url: '/example/TopView' })} />
              <Cell title='Absolute 绝对定位' onClick={() => navigateTo({ url: '/example/Absolute' })} />
              <Cell title='PullView 弹出层' onClick={() => navigateTo({ url: '/example/PullView' })} />
              <Cell title='Modal 弹框' onClick={() => navigateTo({ url: '/example/Modal' })} />
              <Cell title='DropDown 下拉菜单' onClick={() => navigateTo({ url: '/example/DropDown' })} />
              {/* <Cell title='ationSheet 弹出选项' onClick={() => navigateTo({ url: '/example/ShowAtionSheet'})} /> */}
              <Cell title='loading 显示加载动画' onClick={() => navigateTo({ url: '/example/loadingUtil' })} />
              <Cell title='message 消息通知' onClick={() => navigateTo({ url: '/example/message' })} />
              <Cell title='confirm 确认弹框' onClick={() => navigateTo({ url: '/example/confirm' })} />
              <Cell
                title='预览文件'
                onClick={() => previewFile('https://501351981.github.io/vue-office/examples/dist/static/test-files/test.docx')}
              />
            </Cell.Group>
          </GroupList.Item>
          <GroupList.Item title='高级'>
            <Cell.Group>
              {/* <Cell title='Share 分享系统' onClick={() => navigateTo({ url: '/example/Share'})} /> */}
              <Cell title='Chart 图表' onClick={() => navigateTo({ url: '/example/Chart' })} />
              {/* <Cell title='Map 地图, 地图需联网' onClick={() => navigateTo({ url: '/example/Map'})} /> */}
              <Cell title='Sign 签名' onClick={() => navigateTo({ url: '/example/Sign' })} />
            </Cell.Group>
          </GroupList.Item>
        </GroupList>
      </ScrollView>
    </TopView>
  );
};
export default Index;
