export type PageParams = {
  id: number;
  title: string;
};

export type PageData = {
  users: {
    id: number;
    name: string;
    sex: 'boy' | 'girl';
  }[];
};

export type PageBackData = {
  id: number;
  name: string;
};

export const middlewareConfig = {
  mustLogin: true,
  role: [1, 2, 3],
};
