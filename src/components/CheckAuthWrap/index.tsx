import React, { useEffect, useState } from 'react';
import checkAuth from '@/utils/checkAuth';
import useAuthStore from '@/store/useAuthStore';

interface Iprops {
  auth: string | string[];
  /** 默认模式：one 有其中一个符号就通过， all假如传数组，必须全部权限都有   */
  mode?: string;
  children: any;
}

/**
 * 校验权限wrap <CheckAuthWrap auth='/v1/todo' />
 * @param
 * @returns
 */
const CheckAuthWrap = ({ mode = 'one', auth, children }: Iprops) => {
  const [visible, setVisible] = useState(false);
  const auths = useAuthStore(state => state.auths);
  useEffect(() => {
    const bool = checkAuth(auth, mode, auths);
    setVisible(bool);
  }, [auth, auths, mode]);

  if (!visible) {
    return null;
  }
  return children;
};

export default CheckAuthWrap;
