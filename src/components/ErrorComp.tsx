import { Header, ScrollView, Cell, GroupList, TopView, Button } from 'pd-taro-ui';
import { copyLogFileToDownload, getLogFilePath, getLogText } from '@/utils/appLog';
import { Text, View } from '@tarojs/components';
import { useEffect, useState } from 'react';
import Taro from '@tarojs/taro';

const ErrorComp = () => {
  const [logStr, setlogStr] = useState('');
  const [filePath, setFilePath] = useState('');

  const readLog = () => {
    getLogText().then(res => {
      setlogStr(res);
    });
  };

  useEffect(() => {
    readLog();
    setFilePath(getLogFilePath());
  }, []);

  if (process.env.TARO_ENV !== 'rn') return null;
  return (
    <ScrollView>
      <View>
        <Text selectable>错误日志：{filePath}</Text>
      </View>
      <Button
        onClick={async () => {
          const downloadPath = await copyLogFileToDownload();
          Taro.showModal({
            title: '日志已下载到',
            content: downloadPath,
            showCancel: false,
          });
        }}>
        下载日志
      </Button>
      <Text selectable>{logStr}</Text>
    </ScrollView>
  );
};
export default ErrorComp;
