import { create } from 'zustand';
import { persist, devtools } from 'zustand/middleware';
import { mountStoreDevtool } from 'simple-zustand-devtools';

interface AuthState {
  auths: string[];
}
interface AuthAction {}

/** 权限鉴权store */
const useAuthStore = create(
  devtools<AuthState & AuthAction>(
    (set, get) => ({
      auths: [],
    }),
    {
      store: 'useAuthtore',
    },
  ),
);

export default useAuthStore;

if (process.env.NODE_ENV === 'development') {
  mountStoreDevtool('useAuthtore', useAuthStore);
}
