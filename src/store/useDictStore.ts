import { create } from 'zustand';
import { persist, devtools } from 'zustand/middleware';
import { mountStoreDevtool } from 'simple-zustand-devtools';

interface DictItem {
  label: string;
  value: string;
  [key: string]: string;
}

const allDictKeys = ['enum_boolean', 'image_kind'] as const;

type DictKey = (typeof allDictKeys)[number];

type DictState = {
  dicts: {
    [key in DictKey]?: DictItem[];
  };
};
interface DictAction {
  queryAllDict: () => Promise<void>;
}

/** 字典store */
const useDictStore = create(
  devtools<DictState & DictAction>(
    (set, get) => ({
      dicts: {
        image_kind: [],
      },

      queryAllDict: async () => {
        // TODO
        set(state => ({
          dicts: {
            ...state.dicts,
            enum_boolean: [
              { label: '是', value: '1' },
              { label: '否', value: '0' },
            ],
          },
        }));
      },
    }),
    {
      store: 'useDictStore',
    },
  ),
);

export default useDictStore;

if (process.env.NODE_ENV === 'development') {
  mountStoreDevtool('useDictStore', useDictStore);
}
